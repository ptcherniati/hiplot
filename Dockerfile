FROM python:3.8-slim-buster

WORKDIR /app
RUN python3 -m venv venv_hiplot
RUN . venv_hiplot/bin/activate
RUN pip install -U hiplot # not required after first install
COPY run.sh /usr/bin/run.sh
RUN chmod +x /usr/bin/run.sh
EXPOSE 5005

CMD [ "/usr/bin/run.sh" ]
